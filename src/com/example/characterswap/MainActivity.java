package com.example.characterswap;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnClickListener {

	private ImageView slotA, slotB, slotC;
	private int swapDuration = 1000;
	private float grow;
	private State stateA, stateB, stateC;
	int[] locationOfa, locationOfb, locationOfc;
	float diffX, diffY;
	AnimationSet a;

	private static enum Position {
		A, B, C
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		stateA = new State();
		stateB = new State();
		stateC = new State();

		slotA = (ImageView) findViewById(R.id.imageView1);
		stateA.setPosition(Position.A);
		slotA.setOnClickListener(this);
		slotB = (ImageView) findViewById(R.id.imageView2);
		stateB.setPosition(Position.B);
		slotB.setOnClickListener(this);
		slotC = (ImageView) (ImageView) findViewById(R.id.imageView3);
		stateC.setPosition(Position.C);
		slotC.setOnClickListener(this);

	}

	public AnimationSet getAnimation(State from, Position to) {
		int[] destination;
		switch (to) {
		case A:
			destination = locationOfa;
			break;
		case B:
			destination = locationOfb;
			break;
		case C:
			destination = locationOfc;
			break;
		default:
			destination = new int[2];
			break;
		}
		Point offset = from.getOffset();
		Point distance = from.getDistanceTo(destination);
		AnimationSet animation = new AnimationSet(true);
		animation.setDuration(swapDuration);
		animation.setFillAfter(true);
		animation.setInterpolator(new BounceInterpolator());
		TranslateAnimation translate = new TranslateAnimation(offset.x,
				distance.x, offset.y, distance.y);
		ScaleAnimation scale = null;
		if (to == Position.A) {
			scale = new ScaleAnimation(1f, grow, 1f, grow,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		}
		if (scale != null){
			scale.setStartOffset(swapDuration);
			animation.addAnimation(scale);

		}
		animation.addAnimation(translate);
		
		from.setPosition(to);
		return animation;

	}

	private class State {
		public static final int CLOCKWISE = 1;
		public static final int COUNTERCLOCKWISE = 0;
		/**
		 * current position of this object
		 */
		private Position position;
		/**
		 * coordinates of parent view who holds this image, used to calculate
		 * the offset
		 */
		private int[] parent;


		public Position getNext(int direction) {
			if (direction == CLOCKWISE) {
				switch (position) {
				case A:
					return Position.C;
				case B:
					return Position.A;
				case C:
					return Position.B;
				}
			} else {
				switch (position) {
				case A:
					return Position.B;
				case B:
					return Position.C;
				case C:
					return Position.A;
				}
			}
			return null;
		}

		public Point getOffset() {
			Point offset = new Point();
			int[] currentLocation;
			switch (position) {
			case A:
				currentLocation = locationOfa;
				break;
			case B:
				currentLocation = locationOfb;
				break;
			case C:
				currentLocation = locationOfc;
				break;
			default:
				currentLocation = new int[2];
				break;
			}
			// if the original image x is greater than the current location's x
			if (parent[0] > currentLocation[0])
				offset.x = currentLocation[0] - parent[0];
			else
				offset.x = currentLocation[0] - parent[0];
			// if the original image y is greater than the current location's y
			if (parent[1] > currentLocation[1])
				offset.y = (int) (currentLocation[1] - parent[1] - diffY);
			else if (parent[1] < currentLocation[1])
				offset.y = (int) (currentLocation[1] - parent[1]- diffY);
			else
				offset.y = 0;

			return offset;
		}

		public Point getDistanceTo(int[] destination) {
			Point distance = new Point();

			if (parent[0] > destination[0])
				distance.x = destination[0] - parent[0];
			else
				distance.x = destination[0] - parent[0];
			if (parent[1] > destination[1])
				distance.y = (int) (destination[1] - parent[1] -diffY);
			else if (parent[1] < destination[1])
				distance.y = (int) (destination[1] - parent[1] - diffY);
			else
				distance.y = (int) -diffY;

			return distance;
		}

		public void setParentLocation(int[] parentLocation) {
			this.parent = parentLocation;
		}

		public State setPosition(Position position) {
			this.position = position;
			return this;
		}
	};

	public void calculationForSwaps() {
		Resources r = getResources();
		grow = r.getDimension(R.dimen.selected_char_height)
				/ r.getDimension(R.dimen.unselected_char_height);

		diffX = (r.getDimension(R.dimen.selected_char_width) - r
				.getDimension(R.dimen.unselected_char_width)) / 2;
		diffY = (r.getDimension(R.dimen.selected_char_height) - r
				.getDimension(R.dimen.unselected_char_height)) / 2;

		locationOfa = new int[2];
		locationOfb = new int[2];
		locationOfc = new int[2];
		slotA.getLocationOnScreen(locationOfa);
		slotB.getLocationInWindow(locationOfb);
		slotC.getLocationInWindow(locationOfc);
		stateA.setParentLocation(locationOfa);
		stateB.setParentLocation(locationOfb);
		stateC.setParentLocation(locationOfc);

	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		calculationForSwaps();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void rotateClockwise() {
		slotA.startAnimation(getAnimation(stateA,
				stateA.getNext(State.CLOCKWISE)));
		slotB.startAnimation(getAnimation(stateB,
				stateB.getNext(State.CLOCKWISE)));
		slotC.startAnimation(getAnimation(stateC,
				stateC.getNext(State.CLOCKWISE)));
	}

	private void rotateCounterClockwise() {
		slotA.startAnimation(getAnimation(stateA,
				stateA.getNext(State.COUNTERCLOCKWISE)));
		slotB.startAnimation(getAnimation(stateB,
				stateB.getNext(State.COUNTERCLOCKWISE)));
		slotC.startAnimation(getAnimation(stateC,
				stateC.getNext(State.COUNTERCLOCKWISE)));
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.imageView2:
			rotateClockwise();

			break;
		case R.id.imageView3:
			rotateCounterClockwise();
			break;
		default:
			break;
		}

	}

}
